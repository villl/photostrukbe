﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services
{
    public class ArealService : BasePhotostrukService<ArealRepository, Areal, ArealDto>
    {
        public ArealService(ArealRepository repository,
            ILogger<ArealService> logger, IMapper mapper) : base(repository, logger, mapper)
        {
                
        }

        public IEnumerable<ArealImageDto> GetAsocImages()
        {
            return this.mapper.Map<IEnumerable<ArealImageDto>>(this.repository.GetAsocImages());
        }
    }
}
