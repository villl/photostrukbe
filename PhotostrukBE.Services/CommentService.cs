﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class CommentService : CUDService<CommentRepository, Comment, CommentListDto, CommentDto, CommentInputDto>
    {
        public CommentService(CommentRepository repository, ILogger<BaseService<CommentRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {
        }

        public IEnumerable<CommentDto> GetAll(long imageId)
        {
            return this.mapper.Map<IEnumerable<CommentDto>>(this.repository.GetAll(imageId));
        }

        public async Task<CommentDto> Add(CommentInputDto input, long userId)
        {
            input.UserId = userId;
            input.CreatedAt = DateTimeOffset.Now;
            Comment model = await (this.repository.Add(this.mapper.Map<Comment>(input)));
            if (await repository.Save() > 0)
            {
                Comment result = await repository.GetById(model.Id).ConfigureAwait(false);
                return this.mapper.Map<CommentDto>(result);
            }
            throw new HttpStatusCodeException("Data can not be saved", StatusCodes.Status500InternalServerError);
        }

        public async Task<CommentDto> Put(long commentId, long userId, CommentEditDto commentInput)
        {
            if (commentInput == null)
            {
                throw new HttpStatusCodeException("Comment input is null", StatusCodes.Status403Forbidden);
            }

            Comment comment = await repository.GetById(commentId).ConfigureAwait(false);

            if (userId != comment.UserId)
            {
                throw new HttpStatusCodeException("Only owner of the comment can update data", StatusCodes.Status403Forbidden);
            }

            comment.Text = commentInput.Text;
            comment.CreatedAt = DateTimeOffset.Now;

            Comment result = repository.Update(comment);
            if (await repository.Save().ConfigureAwait(false) > 0)
            {
                return mapper.Map<CommentDto>(result);
            }

            throw new HttpStatusCodeException("Can not save item to database", StatusCodes.Status403Forbidden);
        }

        public async Task<CommentDto> Delete(long commentId, long userId)
        {
            Comment comment = await repository.GetById(commentId).ConfigureAwait(false);

            if (userId != comment.UserId)
            {
                throw new HttpStatusCodeException("Only owner of the comment can delete him", StatusCodes.Status403Forbidden);
            }

            Comment result = repository.Delete(comment);
            if (await repository.Save().ConfigureAwait(false) > 0)
            {
                return mapper.Map<CommentDto>(result);
            }

            throw new HttpStatusCodeException("Can not delete item from database", StatusCodes.Status403Forbidden);
        }
    }
}
