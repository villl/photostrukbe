﻿using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PhotostrukBE.Common;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class PhotoService : CUDService<PhotoRepository, Photo, PhotoListDto, PhotoDto, PhotoInputDto>
    {
        private IHostingEnvironment hostingEnvironment;
        private const string PhotoPath = "//photos//";
        private readonly IOptions<CloudinarySettings> cloudinaryConfig;
        private Cloudinary cloudinary;
        public PhotoService(PhotoRepository repository, ILogger<BaseService<PhotoRepository>> logger,
            IMapper mapper, IHostingEnvironment hostingEnvironment, IOptions<CloudinarySettings> cloudinaryConfig) : base(repository, logger, mapper)
        {
            this.hostingEnvironment = hostingEnvironment;
            this.cloudinaryConfig = cloudinaryConfig;

            Account account = new Account
            (
                this.cloudinaryConfig.Value.CloudName,
                this.cloudinaryConfig.Value.ApiKey,
                this.cloudinaryConfig.Value.ApiSecret
            );

            cloudinary = new Cloudinary(account);
        }

        //public FileResult DownloadFile(string filePath, string pathDetail, string fileName)
        //{
        //    IFileProvider provider = new PhysicalFileProvider(filePath + pathDetail);
        //    IFileInfo fileInfo = provider.GetFileInfo(fileName);
        //    if (!fileInfo.Exists)
        //    {
        //        throw new HttpStatusCodeException("File not found", StatusCodes.Status404NotFound);
        //    }
        //    var types = this.GetMimeTypes();
        //    var mimeType = Path.GetExtension(fileName).ToLowerInvariant();
        //    var readStream = fileInfo.CreateReadStream();
        //    return File(readStream, types[mimeType], fileName);
        //}

        public override async Task<PhotoDto> GetById(long photoId)
        {
            Photo photo = await repository.GetById(photoId);

            if (photo == null)
            {
                throw new HttpStatusCodeException("Photo not found", StatusCodes.Status404NotFound);
            }

            return this.mapper.Map<PhotoDto>(photo);
        }

        public IEnumerable<PhotoDto> GetAll(long objectId)
        {
            return this.mapper.Map<IEnumerable<PhotoDto>>(this.repository.GetAll(objectId));
        }

        public async Task<PhotoDto> Add(PhotoInputDto photoInputDto)
        {
            if (photoInputDto == null)
            {
                throw new HttpStatusCodeException("Input object is empty", StatusCodes.Status403Forbidden);
            }

            
            var file = photoInputDto.File;
            Photo photo = new Photo
            {
                Name = file.Name,
                Description = photoInputDto.Description,
                Latitude = photoInputDto.Latitude,
                Longitude = photoInputDto.Longitude,
                ObjectId = photoInputDto.ObjectId,
                UserId = photoInputDto.UserId
            };

            User user = await repository.GetUserById(photoInputDto.UserId);

            if (user == null)
            {
                throw new HttpStatusCodeException("User not exist", StatusCodes.Status404NotFound);
            }

            ImageUploadResult uploadResult = new ImageUploadResult();

            if (file != null && file.Length > 0)
            {
                using (var stream = file.OpenReadStream())
                {
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(file.Name, stream),
                        Transformation = new Transformation()
                    };

                    uploadResult = await cloudinary.UploadAsync(uploadParams).ConfigureAwait(false);

                    photo.PublicId = uploadResult.PublicId;
                    photo.Url = uploadResult.Url.ToString();
                    photo.CreatedAt = DateTimeOffset.Now;
                    photo = await repository.Add(photo);
                    
                    if (await repository.Save().ConfigureAwait(false) == 0)
                    {
                        throw new HttpStatusCodeException("Photo can not be saved", StatusCodes.Status500InternalServerError);
                    }
                }
                return mapper.Map<PhotoDto>(photo);
            }
            else
            {
                throw new HttpStatusCodeException("No file added", StatusCodes.Status403Forbidden);
            }
        }

        public async Task Delete(long userId, long photoId)
        {
            //Avatar avatar = await repository.GetAvatar(userId);
            User user = await repository.GetUserById(userId);

            if (user == null)
            {
                throw new HttpStatusCodeException("User not found", StatusCodes.Status404NotFound);
            }

            Photo photo = await repository.GetById(photoId);

            if (photo == null)
            {
                throw new HttpStatusCodeException("Photo not found", StatusCodes.Status404NotFound);
            }

            //var delDerivedResParams = new DelDerivedResParams()
            //{
            //    PublicId = user.Avatar.PublicId
            //};
            //DelDerivedResResult delDerivedResResult = await cloudinary.DeleteDerivedResourcesAsync(delDerivedResParams).ConfigureAwait(false);

            var deletionParams = new DeletionParams(photo.PublicId);
            var result = await cloudinary.DestroyAsync(deletionParams).ConfigureAwait(false);

            if (result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                repository.Delete(photo);
                if (await repository.Save().ConfigureAwait(false) == 0)
                {
                    throw new HttpStatusCodeException("Photo can not be deleted", StatusCodes.Status500InternalServerError);
                }
            }
            else
            {
                throw new HttpStatusCodeException("Avatar cannot be deleted", StatusCodes.Status403Forbidden);
            }
        }

        //public override async Task<PhotoDto> Add(PhotoInputDto photoInputDto)
        //{
        //    var file = photoInputDto.File;

        //    User user = await repository.GetUserById(photoInputDto.UserId);

        //    if (user == null)
        //    {
        //        throw new HttpStatusCodeException("User not exist", StatusCodes.Status404NotFound);
        //    }

        //    string guid = Guid.NewGuid().ToString();
        //    var mimeType = Path.GetExtension(file.FileName).ToLowerInvariant();
        //    if (file != null && file.Length > 0)
        //    {
        //        string path = PhotoPath + guid + mimeType;
        //        Photo photo = new Photo();
        //        using (FileStream fs = File.Create(hostingEnvironment.WebRootPath + path))
        //        {
        //            photo.Name = guid + mimeType;
        //            photo.Path = PhotoPath;
        //            photo.CreatedAt = DateTimeOffset.Now;
        //            photo.UserId = user.Id;
        //            photo.ImageId = photoInputDto.ImageId;

        //            await repository.Add(photo);
        //            if (await repository.Save() == 0)
        //            {
        //                throw new HttpStatusCodeException("Photo can not be saved", StatusCodes.Status500InternalServerError);
        //            }
        //            await file.CopyToAsync(fs);
        //            await fs.FlushAsync();
        //        }
        //        return this.mapper.Map<PhotoDto>(photo);
        //    }
        //    throw new HttpStatusCodeException("Something went wrong", StatusCodes.Status500InternalServerError);
        //}

        //public async Task Delete(long photoId)
        //{
        //    Photo photo = await repository.GetById(photoId);

        //    if (photo == null)
        //    {
        //        throw new HttpStatusCodeException("Photo not found in database", StatusCodes.Status404NotFound);
        //    }

        //    if (!File.Exists(hostingEnvironment.WebRootPath + photo.Path))
        //    {
        //        throw new HttpStatusCodeException("Photo not found on disk", StatusCodes.Status404NotFound);
        //    }

        //    File.Delete(hostingEnvironment.WebRootPath + photo.Path);
        //    repository.Delete(photo);
        //    await repository.Save();
        //}

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.ms-excel"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}
