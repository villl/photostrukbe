﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System.Collections.Generic;

namespace PhotostrukBE.Services
{
    public class ComponentService : 
        BasePhotostrukService<ComponentRepository, Component, ComponentDto>
    {
        public ComponentService(ComponentRepository repository,
            ILogger<ComponentService> logger, IMapper mapper) : base(repository, logger, mapper)
        {

        }

        public IEnumerable<ComponentImageDto> GetAsocImages()
        {
            return this.mapper.Map<IEnumerable<ComponentImageDto>>(this.repository.GetAsocImages());
        }
    }
}
