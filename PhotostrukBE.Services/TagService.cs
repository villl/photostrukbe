﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class TagService : CUDService<TagRepository, Tag, TagDto, TagDto, TagInputDto>
    {
        public TagService(TagRepository repository, ILogger<BaseService<TagRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {
        }

        public IEnumerable<TagDto> GetAll(long imageId)
        {
            var result = this.repository.GetAll(imageId).ToList();
                result.AddRange(this.repository.GetPhotostrukAll(imageId));
            return this.mapper.Map<IEnumerable<TagDto>>(result);
        }

        public async Task<TagDto> Add(TagInputDto input, long userId)
        {
            input.UserId = userId;
            Tag model = await (this.repository.Add(this.mapper.Map<Tag>(input)));
            
            if (await repository.Save() > 0)
            {
                return this.mapper.Map<TagDto>(model);
            }
            throw new HttpStatusCodeException("Data can not be saved", StatusCodes.Status500InternalServerError);
        }
    }
}
