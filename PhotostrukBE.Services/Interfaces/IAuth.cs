﻿using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Services.Interfaces
{
    public interface IAuth
    {
        Task<TokenDto> Login(LoginDto loginDto);
        Task<TokenDto> Register(RegisterDto registerDto);
        Task ChangePassword(RecoverPasswordDto recoverPasswordDto, long userId);
    }
}
