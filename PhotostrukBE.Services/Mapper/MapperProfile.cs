﻿using AutoMapper;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;

namespace PhotostrukBE.Services.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Areal, ArealDto>();
            CreateMap<Areal, ArealListDto>();
            CreateMap<Component, ComponentDto>();
            CreateMap<Component, ComponentListDto>();
            CreateMap<Village, VillageDto>();
            CreateMap<Village, VillageListDto>();
            CreateMap<VillageAsocGpsTimePeriod, VillageAsocGpsTimePeriodDto>();
            CreateMap<GpsTimePeriod, GpsTimePeriodDto>();
            CreateMap<Image, ImageDto>();
            CreateMap<VillageImage, VillageImageDto>();
            CreateMap<ArealImage, ArealImageDto>();
            CreateMap<ComponentImage, ComponentImageDto>();

            CreateMap<User, UserDto>();
            CreateMap<User, UserListDto>();
            CreateMap<Comment, CommentDto>();
            CreateMap<Comment, CommentListDto>();
            CreateMap<CommentOutput, CommentDto>();
            CreateMap<Tag, TagDto>();
            CreateMap<Like, LikeDto>();
            CreateMap<Photo, PhotoDto>();
            CreateMap<Photo, PhotoListDto>();

            CreateMap<CommentInputDto, Comment>();
            CreateMap<LikeInputDto, Like>();
            CreateMap<TagInputDto, Tag>();
        }
    }
}
