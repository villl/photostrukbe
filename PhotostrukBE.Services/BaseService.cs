﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class BaseService<TRepository, TModel, TListDto, TDetailDto> : BaseService<TRepository>
        where TRepository : BaseRepository<TModel>
        where TModel : class, IBaseModel, new()
        where TListDto : class
        where TDetailDto : class
    {

        public BaseService(TRepository repository, ILogger<BaseService<TRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {

        }

        public virtual IEnumerable<TListDto> GetAll()
        {
            return this.mapper.Map<IEnumerable<TListDto>>(this.repository.GetAll());
        }

        public async virtual Task<TDetailDto> GetById(long id)
        {
            return this.mapper.Map<TDetailDto>(await this.repository.GetById(id));
        }

    }

    public class BaseService<TRepository> : BaseService where TRepository : BaseRepository
    {
        protected readonly TRepository repository;

        public BaseService(TRepository repository, ILogger<BaseService<TRepository>> logger, IMapper mapper) : base(logger, mapper)
        {
            this.repository = repository;
        }
    }


    public class BaseService
    {
        protected readonly ILogger<BaseService> logger;
        protected readonly IMapper mapper;

        public BaseService(ILogger<BaseService> logger, IMapper mapper)
        {
            this.logger = logger;
            this.mapper = mapper;
        }
    }
}
