﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System.Collections.Generic;

namespace PhotostrukBE.Services
{
    public class VillageService : 
        BasePhotostrukService<VillageRepository, Village, VillageListDto>
    {
        public VillageService(VillageRepository repository,
            ILogger<VillageService> logger, IMapper mapper) : base(repository, logger, mapper)
        {

        }

        public IEnumerable<VillageImageDto> GetAsocImages()
        {
            return this.mapper.Map<IEnumerable<VillageImageDto>>(this.repository.GetAsocImages());
        }

        public IEnumerable<VillageAsocGpsTimePeriodDto> GetAsocGpsTimePeriod()
        {
            return this.mapper.Map<IEnumerable<VillageAsocGpsTimePeriodDto>>(this.repository.GetAsocGpsTimePeriod());
        }

        public IEnumerable<GpsTimePeriodDto> GetGpsTimePeriods()
        {
            return this.mapper.Map<IEnumerable<GpsTimePeriodDto>>(this.repository.GetGpsTimePeriods());
        }
    }
}
