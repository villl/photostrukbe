﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class CUDService<TRepository, TModel, TListDto, TDetailDto, TInputDto> : BaseService<TRepository, TModel, TListDto, TDetailDto>
        where TRepository : CUDRepository<TModel>
        where TModel : class, IBaseModel, new()
        where TListDto : class
        where TDetailDto : class
        where TInputDto : class
    {
        public CUDService(TRepository repository, ILogger<BaseService<TRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {
        }

        public async virtual Task<TDetailDto> Add(TInputDto input)
        {
            TModel model = await (this.repository.Add(this.mapper.Map<TModel>(input)));
            if (await repository.Save() > 0)
            {
                return this.mapper.Map<TDetailDto>(model);
            }
            throw new HttpStatusCodeException("Data can not be saved", StatusCodes.Status500InternalServerError);
        }
    }
}
