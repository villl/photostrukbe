﻿namespace PhotostrukBE.Services.Dtos
{
    public class GpsCoordinateDto : BaseDto
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
