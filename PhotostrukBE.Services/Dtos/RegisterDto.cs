﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class RegisterDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^[!-@a-žA-Ž0-9]{8,30}$", ErrorMessage = "Password must contail at least 8 characters.")]
        public string Password { get; set; }
    }
}
