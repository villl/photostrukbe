﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class TokenDto
    {
        public string Token { get; set; }
    }
}
