﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class RecoverPasswordDto
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z0-9]{8,30}$", ErrorMessage = "Password must contail at least 8 characters.")]
        public string NewPassword { get; set; }
    }
}
