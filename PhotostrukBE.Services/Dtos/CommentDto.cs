﻿using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class CommentDto
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public UserListDto User { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }

    public class CommentListDto
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
    }

    public class CommentInputDto
    {
        [Required]
        public string Text { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long UserId { get; set; }
        public long ImageId { get; set; }
        public long? ReferenceTo { get; set; }
    }

    public class CommentEditDto
    {
        [Required]
        public string Text { get; set; }
    }
}
