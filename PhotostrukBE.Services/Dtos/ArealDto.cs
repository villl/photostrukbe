﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class ArealDto : BaseDto
    {
        public int? VillageId { get; set; }
        public string Description { get; set; }
        public string Degree { get; set; }
        public string Type { get; set; }
        public string MaterialRecycling { get; set; }
        public string DisposalOfResidues { get; set; }
        public string SurfaceAdjusment { get; set; }
        public string TerrainRelicts { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }

    public class ArealListDto : BaseDto
    {
        public string Description { get; set; }
        public string DescriptionGerman { get; set; }
    }
}
