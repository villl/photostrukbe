﻿using System.ComponentModel.DataAnnotations;

namespace PhotostrukBE.Services.Dtos
{
    public class TagDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class TagInputDto
    {
        [Required]
        public string Name { get; set; }
        public long ImageId { get; set; }
        public long UserId { get; set; }
    }
}
