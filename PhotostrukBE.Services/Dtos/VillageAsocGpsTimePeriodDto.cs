﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class VillageAsocGpsTimePeriodDto
    {
        public int VillageId { get; set; }
        public int GpsTimePeriodId { get; set; }
    }
}
