﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class ComponentImageDto : BaseDto
    {
        public int ComponentId { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
