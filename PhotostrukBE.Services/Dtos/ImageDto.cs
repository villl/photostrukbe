using System;
using System.IO;

namespace PhotostrukBE.Services.Dtos
{
    public partial class ImageDto : BaseDto
    {
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public Stream FileStream { get; set; }
        public string ContentType { get; set; }
    }
}