﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class ImageTagsDto
    {
        public int ImageId { get; set; }
        public string Tag { get; set; } 
    }
}
