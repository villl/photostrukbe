﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class LikeDto
    {
        public bool IsLiked { get; set; }
    }

    public class LikeInputDto
    {
        public bool IsLiked { get; set; }
        public long UserId { get; set; }
        public long CommentId { get; set; }
    }
}
