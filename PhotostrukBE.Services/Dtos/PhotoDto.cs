﻿using Microsoft.AspNetCore.Http;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;

namespace PhotostrukBE.Services.Dtos
{
    public class PhotoDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PublicId { get; set; }
        public string Url { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long ObjectId { get; set; }
        public long UserId { get; set; }
    }

    public class PhotoListDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PublicId { get; set; }
        public string Url { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long ObjectId { get; set; }
        public long UserId { get; set; }
    }

    public class PhotoInputDto
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public long ObjectId { get; set; }
        [Required(ErrorMessage = "No file selected.")]
        [DataType(DataType.Upload)]
        [MaxFileSize(2 * 1024 * 1024)]
        [AllowedExtensions(new string[] { ".jpg", ".jpeg", ".png" })]
        public IFormFile File { get; set; }
        public long UserId { get; set; }
    }
}
