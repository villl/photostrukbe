﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class ImageService : BasePhotostrukService<ImageRepository>
    {
        private IHostingEnvironment hostingEnvironment;
        private const string ImagePath = "//web_120px//";
        public ImageService(ImageRepository repository, ILogger<BasePhotostrukService<ImageRepository>> logger,
            IMapper mapper, IHostingEnvironment hostingEnvironment) : base(repository, logger, mapper)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        public async Task<ImageDto> GetById(long imageId)
        {
            Image image = await repository.GetById(imageId);

            if (image == null)
            {
                throw new HttpStatusCodeException("Image not found", StatusCodes.Status404NotFound);
            }

            string[] splitedPath = image.Path.Split('/');

            if (!File.Exists(hostingEnvironment.WebRootPath + ImagePath + image.Path))
            {
                logger.LogError(hostingEnvironment.WebRootPath + ImagePath + image.Path);
                throw new HttpStatusCodeException("Image not found on disk", StatusCodes.Status404NotFound);
            }

            IFileProvider provider = new PhysicalFileProvider(hostingEnvironment.WebRootPath + 
                ImagePath + splitedPath[0]);
            IFileInfo fileInfo = provider.GetFileInfo(splitedPath[1]);

            ImageDto imageDto = new ImageDto();
            imageDto.FileStream = fileInfo.CreateReadStream();

            var types = this.GetMimeTypes();
            var mimeType = Path.GetExtension(image.Path).ToLowerInvariant();
            imageDto.ContentType = types[mimeType];
            return imageDto;
        }

        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain"},
                {".pdf", "application/pdf"},
                {".doc", "application/vnd.ms-word"},
                {".docx", "application/vnd.ms-word"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsx", "application/vnd.ms-excel"},
                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".gif", "image/gif"},
                {".csv", "text/csv"}
            };
        }
    }
}
