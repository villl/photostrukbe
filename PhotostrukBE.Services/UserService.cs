﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;

namespace PhotostrukBE.Services
{
    public class UserService : BaseService<UserRepository, User, UserListDto, UserDto>
    {
        public UserService(UserRepository repository, ILogger<BaseService<UserRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {
        }


    }
}
