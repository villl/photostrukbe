﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using PhotostrukBE.Common;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using PhotostrukBE.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class AuthService : CUDService<AuthRepository, User, UserDto, UserDto, UserDto>, IAuth
    {
        private readonly IConfiguration configuration;

        public AuthService(AuthRepository repository, ILogger<AuthService> logger, IMapper mapper,
            IConfiguration configuration) : base(repository, logger, mapper)
        {
            this.configuration = configuration;
        }

        public async Task<TokenDto> Login(LoginDto loginDto)
        {
            User user = await this.repository.GetUserByEmail(loginDto.Email);

            if (user == null)
            {
                throw new HttpStatusCodeException("User not exist", StatusCodes.Status404NotFound);
            }

            if (VerifyPasswordHash(loginDto.Password, user.PasswordHash, user.PasswordSalt))
            {
                logger.LogInformation("User login");

                //foreach (var item in user.UserEvent)
                //{
                //    await hubContext.Groups.AddToGroupAsync(hubConnection.ConnectionId, item.EventId.ToString());
                //    await hubContext.Clients.Group(item.EventId.ToString()).SendAsync(user.NickName + " joined.");
                //}

                return this.GenerateToken(user);
            }

            throw new HttpStatusCodeException("Wrong password", StatusCodes.Status403Forbidden);
        }



        public async Task<TokenDto> Register(RegisterDto registerDto)
        {
            User user = new User();
            if (await this.repository.IsUserExists(registerDto.Email).ConfigureAwait(false))
            {
                throw new HttpStatusCodeException("User already exists", StatusCodes.Status403Forbidden);
            }

            User resultUser = await CreateUser(registerDto.FirstName, registerDto.LastName,
                registerDto.Password, registerDto.Email).ConfigureAwait(false);

            return this.GenerateToken(user);
        }

        public async Task<TokenDto> ExternalLogin(ExternalLoginDto externalLoginDto)
        {
            if (externalLoginDto == null)
            {
                throw new HttpStatusCodeException("external Login input is null", StatusCodes.Status403Forbidden);
            }

            User user = await GetOrCreateExternalUser("Facebook", externalLoginDto).ConfigureAwait(false);

            if (user == null)
            {
                throw new HttpStatusCodeException("Unable to create user", StatusCodes.Status403Forbidden);
            }

            return this.GenerateToken(user);
        }

        public async Task<User> GetOrCreateExternalUser(string provider, ExternalLoginDto externalLoginDto)
        {
            User user;
            user = await repository.GetUserByEmail(externalLoginDto.Email).ConfigureAwait(false);

            if (user == null)
            {
                // No user exists with this email address, we create a new one
                user = await CreateUser(Guid.NewGuid().ToString(), externalLoginDto.Id, externalLoginDto.Email,
                    externalLoginDto.FirstName, externalLoginDto.LastName, provider).ConfigureAwait(false);
            }

            return user;
        }

        private async Task<User> CreateUser(string password, long id, string email, string firstName, string lastName, string provider)
        {

            byte[] passwordHash, passwordSalt;
            this.CreatePasswordHash(password, out passwordHash, out passwordSalt);

            User user = new User
            {
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                FirstName = firstName,
                LastName = lastName,
                Provider = provider
            };


            User resultUser = await this.repository.Add(user).ConfigureAwait(false);
            if (await this.repository.Save().ConfigureAwait(false) > 0)
            {
                logger.LogInformation("User created a new account with password.");
                return resultUser;
            }
            logger.LogError("User not created");
            return null;
        }

        private async Task<User> CreateUser(string firstName, string lastName, string password, string email)
        {

            byte[] passwordHash, passwordSalt;
            this.CreatePasswordHash(password, out passwordHash, out passwordSalt);

            User user = new User
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            };


            User resultUser = await this.repository.Add(user).ConfigureAwait(false);
            if (await this.repository.Save().ConfigureAwait(false) > 0)
            {
                logger.LogInformation("User created a new account with password.");
                return resultUser;
            }
            logger.LogError("User not created");
            return null;
        }

        private TokenDto GenerateToken(User user)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes
                (configuration.GetSection("JwtSettings:Secret").Value));
            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var tokeOptions = new JwtSecurityToken(
                claims: new List<Claim> {
                    new Claim(Const.UserID, user.Id.ToString()),
                    new Claim(Const.Role, "User"),
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                },
                expires: DateTime.Now.AddHours(20),
                signingCredentials: signinCredentials
            );

            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);
            return new TokenDto { Token = tokenString };
        }

        public async Task ChangePassword(RecoverPasswordDto recoverPasswordDto, long userId)
        {
            byte[] passwordHash, passwordSalt;
            User user = await repository.GetById(userId);

            if (!VerifyPasswordHash(recoverPasswordDto.OldPassword, user.PasswordHash, user.PasswordSalt))
            {
                throw new HttpStatusCodeException("Wrong password", StatusCodes.Status403Forbidden);
            }

            this.CreatePasswordHash(recoverPasswordDto.NewPassword, out passwordHash, out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            repository.Update(user);
            await repository.Save();
            logger.LogInformation("Password has been changed");
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
