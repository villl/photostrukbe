﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Services
{
    public class BasePhotostrukService<TRepository, TModel, TListDto> : BasePhotostrukService<TRepository>
        where TRepository : BasePhotostrukRepository<TModel>
        where TModel : class, IBaseModel, new()
        where TListDto : class
    {

        public BasePhotostrukService(TRepository repository, ILogger<BasePhotostrukService<TRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {

        }

        public virtual IEnumerable<TListDto> GetAll(string cultureInfo)
        {
            return this.mapper.Map<IEnumerable<TListDto>>(this.repository.GetAll(cultureInfo));
        }
    }

    public class BasePhotostrukService<TRepository> : BaseService where TRepository : BasePhotostrukRepository
    {
        protected readonly TRepository repository;

        public BasePhotostrukService(TRepository repository, ILogger<BasePhotostrukService<TRepository>> logger, IMapper mapper) : base(logger, mapper)
        {
            this.repository = repository;
        }
    }

    public class BasePhotostrukService
    {
        protected readonly ILogger<BasePhotostrukService> logger;
        protected readonly IMapper mapper;

        public BasePhotostrukService(ILogger<BasePhotostrukService> logger, IMapper mapper)
        {
            this.logger = logger;
            this.mapper = mapper;
        }
    }
}
