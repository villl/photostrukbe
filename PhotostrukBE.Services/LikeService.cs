﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Services
{
    public class LikeService : CUDService<LikeRepository, Like, LikeDto, LikeDto, LikeInputDto>
    {
        public LikeService(LikeRepository repository, ILogger<BaseService<LikeRepository>> logger, IMapper mapper) : base(repository, logger, mapper)
        {
        }

        public virtual IEnumerable<LikeDto> GetAll(long commentId)
        {
            return this.mapper.Map<IEnumerable<LikeDto>>(this.repository.GetAll());
        }

        public async Task<CommentDto> Add(LikeInputDto input, long userId)
        {
            if (!repository.IsCommentExist(input.CommentId))
            {
                throw new HttpStatusCodeException("Comment not found", StatusCodes.Status404NotFound);
            }

            Like model = this.repository.GetLiked(input.CommentId, userId);
            input.UserId = userId;

            if (model == null)
            {
                model = await (this.repository.Add(this.mapper.Map<Like>(input)));
            }
            else
            {
                model.IsLiked = input.IsLiked;
                model.CreatedAt = DateTimeOffset.Now;
                model = this.repository.Update(model);
            }
            
            if (await repository.Save() > 0)
            {
                return this.mapper.Map<CommentDto>(await repository.GetCommentById(model.CommentId));
            }
            throw new HttpStatusCodeException("Data can not be saved", StatusCodes.Status500InternalServerError);
        }
    }
}
