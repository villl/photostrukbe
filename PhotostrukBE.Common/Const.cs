﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Common
{
    public static class Const
    {
        public const string UserID = "userID";
        public const string Role = "role";
        public const string GermanCultureInfo = "DE";
    }
}
