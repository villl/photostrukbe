﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Common.Exceptions
{
    public class HttpStatusCodeException : Exception
    {
        public int StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";

        public HttpStatusCodeException(int statusCode)
        {
            this.StatusCode = statusCode;
        }

        public HttpStatusCodeException(string message) : this(message, 500) { }

        public HttpStatusCodeException(Exception ex) : this(ex.Message, 500) { }

        public HttpStatusCodeException(string message, int statusCode) : base(message)
        {
            this.StatusCode = statusCode;
        }

        public HttpStatusCodeException(Exception ex, int statusCode) : base(ex.Message)
        {
            this.StatusCode = statusCode;
        }
    }
}
