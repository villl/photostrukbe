﻿using LazyCache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Common;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Services;

namespace PhotostrukBE.WebAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
    }

    public class BaseController<TService> : BaseController where TService : BaseService
    {
        protected readonly TService service;
        protected readonly IAppCache cache;
        public BaseController(TService service, IAppCache cache)
        {
            this.service = service;
            this.cache = cache;
        }

        protected long GetUserId()
        {
            if (long.TryParse(User.FindFirst(Const.UserID).Value, out long result))
            {
                return result;
            }
            throw new HttpStatusCodeException("Id is not a number", StatusCodes.Status403Forbidden);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}
