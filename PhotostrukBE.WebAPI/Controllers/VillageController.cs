﻿using LazyCache;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Common;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    public class VillageController : BaseController<VillageService>
    {
        public VillageController(VillageService service, IAppCache cache) : base(service, cache)
        {

        }

        // GET: api/Village
        [HttpGet("{cultureInfo}")]
        public IEnumerable<VillageListDto> Get(string cultureInfo)
        {
            if (cultureInfo.Trim().ToUpper() == Const.GermanCultureInfo)
            {
                return cache.GetOrAdd("Village.GetAllDE", () => service.GetAll(cultureInfo));
            }
            else
            {
                return cache.GetOrAdd("Village.GetAll", () => service.GetAll(cultureInfo));
            }
        }

        // GET: api/Village/images
        [HttpGet("images")]
        public IEnumerable<VillageImageDto> GetAsocImages()
        {
            return cache.GetOrAdd("Village.GetAsocImages", () => service.GetAsocImages());
        }

        //[HttpGet("AsocGpsTimePeriod")]
        //public IEnumerable<VillageAsocGpsTimePeriodDto> GetAsocGpsTimePeriod()
        //{
        //    return service.GetAsocGpsTimePeriod();
        //}

        //[HttpGet("GpsTimePeriods")]
        //public IEnumerable<GpsTimePeriodDto> GetGpsTimePeriods()
        //{
        //    return service.GetGpsTimePeriods();
        //}
    }
}
