﻿using LazyCache;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    public class UserController : BaseController<UserService>
    {

        public UserController(UserService service, IAppCache cache) : base(service, cache)
        {
        }

        [HttpGet]
        public IEnumerable<UserListDto> Get()
        {
            return cache.GetOrAdd("User.GetAll", () => this.service.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> Get(long id)
        {
            return await this.service.GetById(id).ConfigureAwait(false);
        }

        //[HttpPut("{id}")]
        //public async Task<UserDto> Put(long id, UserInputDto user)
        //{
        //    long userId = this.GetUserId();
        //    if (id != userId)
        //    {
        //        throw new HttpStatusCodeException("Ids of user are not match", StatusCodes.Status403Forbidden);
        //    }

        //    return await service.Put(id, user).ConfigureAwait(false);
        //}
    }
}
