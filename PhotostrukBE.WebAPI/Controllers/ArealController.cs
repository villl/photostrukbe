﻿using LazyCache;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Common;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    public class ArealController : BaseController<ArealService>
    {
        public ArealController(ArealService service, IAppCache cache) : base(service, cache)
        {

        }

        // GET: api/Areal
        [HttpGet("{cultureInfo}")]
        public IEnumerable<ArealDto> Get(string cultureInfo)
        {
            if (cultureInfo.Trim().ToUpper() == Const.GermanCultureInfo)
            {
                return cache.GetOrAdd("Areal.GetAllDE", () => service.GetAll(cultureInfo));
            }
            else
            {
                return cache.GetOrAdd("Areal.GetAll", () => service.GetAll(cultureInfo));
            }
        }

        // GET: api/Areal/images
        [HttpGet("images")]
        public IEnumerable<ArealImageDto> GetAsocImage()
        {
            return cache.GetOrAdd("Component.GetAsocImages", () => service.GetAsocImages());
        }
    }
}
