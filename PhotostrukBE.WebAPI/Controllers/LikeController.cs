﻿using LazyCache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    [Authorize]
    public class LikeController : BaseController<LikeService>
    {
        public LikeController(LikeService service, IAppCache cache) : base(service, cache)
        {
        }

        [HttpGet("{commentId}")]
        public IEnumerable<LikeDto> Get(long commentId)
        {
            return service.GetAll(commentId);
        }

        // POST: api/Like
        [HttpPost]
        public async Task<CommentDto> Post([FromBody] LikeInputDto like)
        {
            return await this.service.Add(like, GetUserId()).ConfigureAwait(false);
        }
    }
}
