﻿using LazyCache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    [Authorize]
    public class CommentController : BaseController<CommentService>
    {

        public CommentController(CommentService service, IAppCache cache) : base(service, cache)
        {
        }

        [HttpGet]
        public IEnumerable<CommentListDto> Get()
        {
            return this.service.GetAll();
        }

        [HttpGet("image/{imageId}")]
        public IEnumerable<CommentDto> GetAsocImage(long imageId)
        {
            return this.service.GetAll(imageId);
        }

        [HttpGet("{commentId}")]
        public async Task<ActionResult<CommentDto>> Get(long commentId)
        {
            return await this.service.GetById(commentId).ConfigureAwait(false);
        }

        // POST: api/Comment
        [HttpPost]
        public async Task<CommentDto> Post([FromBody] CommentInputDto comment)
        {
            return await this.service.Add(comment, GetUserId()).ConfigureAwait(false);
        }

        // PUT: api/Comment/5
        [HttpPut("{commentId}")]
        public async Task<CommentDto> Put(long commentId, [FromBody] CommentEditDto comment)
        {
            return await this.service.Put(commentId, GetUserId(), comment).ConfigureAwait(false);
        }

        // DELETE: api/Comment/5
        [HttpDelete("{commentId}")]
        public async Task<CommentDto> Delete(long commentId)
        {
            return await this.service.Delete(commentId, GetUserId()).ConfigureAwait(false);
        }
    }
}
