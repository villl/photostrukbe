﻿using LazyCache;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    public class ImageController : BaseController<ImageService>
    {
        public ImageController(ImageService service, IAppCache cache) : base(service, cache)
        {
        }

        [HttpGet("{imageId}")]
        public async Task<IActionResult> Get(long imageId)
        {
            ImageDto imageDto = await service.GetById(imageId).ConfigureAwait(false);
            return File(imageDto.FileStream, imageDto.ContentType);
        } 
    }
}
