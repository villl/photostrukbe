﻿using LazyCache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    [Authorize]
    public class TagController : BaseController<TagService>
    {
        public TagController(TagService service, IAppCache cache) : base(service, cache)
        {
        }

        [AllowAnonymous]
        [HttpGet("{imageId}")]
        public IEnumerable<TagDto> Get(long imageId)
        {
            return service.GetAll(imageId);
        }

        // POST: api/Tag
        [HttpPost]
        public async Task<TagDto> Post([FromBody] TagInputDto tag)
        {

            return await this.service.Add(tag, GetUserId()).ConfigureAwait(false);
        }
    }
}
