﻿using LazyCache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    [Authorize]
    public class PhotoController : BaseController<PhotoService>
    {

        public PhotoController(PhotoService service, IAppCache cache) : base(service, cache)
        {
        }

        [HttpGet]
        public IEnumerable<PhotoListDto> Get()
        {
            return cache.GetOrAdd("User.GetAll", () => this.service.GetAll());
        }

        //[HttpGet("{photoId}")]
        //public async Task<IActionResult> Get(long photoId)
        //{
        //    var result = await this.service.GetById(photoId).ConfigureAwait(false);
        //    return File(result.FileStream, result.ContentType);
        //}

        [AllowAnonymous]
        [HttpGet("object/{objectId}")]
        public IEnumerable<PhotoDto> GetAsocImage(long objectId)
        {
            return this.service.GetAll(objectId);
        }

        [HttpPost]
        public async Task<PhotoDto> Add([FromForm]PhotoInputDto photoInputDto)
        {
            photoInputDto.UserId = GetUserId();
            return await this.service.Add(photoInputDto).ConfigureAwait(false);
        }

        [HttpDelete("{photoId}")]
        public async Task<IActionResult> Delete(long photoId)
        {
            long userId = this.GetUserId();
            await service.Delete(userId, photoId).ConfigureAwait(false);
            return Ok();
        }
    }
}
