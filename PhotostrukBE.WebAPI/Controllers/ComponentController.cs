﻿using LazyCache;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Common;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;

namespace PhotostrukBE.WebAPI.Controllers
{
    public class ComponentController : BaseController<ComponentService>
    {
        public ComponentController(ComponentService service, IAppCache cache) : base(service, cache)
        {

        }

        // GET: api/Component
        [HttpGet("{cultureInfo}")]
        public IEnumerable<ComponentDto> Get(string cultureInfo)
        {

            if (cultureInfo.Trim().ToUpper() == Const.GermanCultureInfo)
            {
                return cache.GetOrAdd("Component.GetAllDE", () => service.GetAll(cultureInfo));
            }
            else
            {
                return cache.GetOrAdd("Component.GetAll", () => service.GetAll(cultureInfo));
            }
                
        }

        // GET: api/Component/images
        [HttpGet("images")]
        public IEnumerable<ComponentImageDto> GetAsocImages()
        {
            return cache.GetOrAdd("Component.GetAsocImages", () => service.GetAsocImages());
        }
    }
}
