﻿using LazyCache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PhotostrukBE.Data.Models;
using PhotostrukBE.Services;
using PhotostrukBE.Services.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.WebAPI.Controllers
{
    public class AuthController : BaseController<AuthService>
    {
        public AuthController(AuthService service, IAppCache cache) : base(service, cache)
        {
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<TokenDto> Login([FromBody] LoginDto loginDto)
        {
            return await this.service.Login(loginDto).ConfigureAwait(true);
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<TokenDto> Register([FromBody] RegisterDto registerDto)
        {
            return await this.service.Register(registerDto).ConfigureAwait(false);
        }

        [HttpPost("externallogin")]
        [AllowAnonymous]
        public async Task<TokenDto> ExternalLogin([FromBody] ExternalLoginDto externalLoginDto)
        {
            return await this.service.ExternalLogin(externalLoginDto).ConfigureAwait(false);
        }

        [HttpPost("changepassword")]
        public async Task<IActionResult> ChangePassword([FromBody] RecoverPasswordDto recoverPasswordDto)
        {
            await this.service.ChangePassword(recoverPasswordDto, GetUserId()).ConfigureAwait(false);
            return Ok();
        }
    }
}
