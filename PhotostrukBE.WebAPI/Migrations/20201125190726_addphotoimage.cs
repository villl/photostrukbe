﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotostrukBE.WebAPI.Migrations
{
    public partial class addphotoimage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ImageId",
                table: "Photo",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Photo");
        }
    }
}
