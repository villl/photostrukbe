﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotostrukBE.WebAPI.Migrations
{
    public partial class editPhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "Photo");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Photo",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Latitude",
                table: "Photo",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "Longitude",
                table: "Photo",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<long>(
                name: "ObjectId",
                table: "Photo",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Photo");

            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "Photo");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "Photo");

            migrationBuilder.DropColumn(
                name: "ObjectId",
                table: "Photo");

            migrationBuilder.AddColumn<long>(
                name: "ImageId",
                table: "Photo",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
