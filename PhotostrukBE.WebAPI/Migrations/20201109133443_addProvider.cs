﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotostrukBE.WebAPI.Migrations
{
    public partial class addProvider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Provider",
                table: "User",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Provider",
                table: "User");
        }
    }
}
