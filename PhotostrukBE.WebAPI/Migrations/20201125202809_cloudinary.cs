﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotostrukBE.WebAPI.Migrations
{
    public partial class cloudinary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PublicId",
                table: "Photo",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Photo",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PublicId",
                table: "Photo");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "Photo");
        }
    }
}
