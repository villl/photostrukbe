﻿using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Data.Interfaces;
using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class CUDRepository<TModel> : BaseRepository<TModel> where TModel : class, IBaseModel
    {
        public CUDRepository(DataContext db) : base(db)
        {
        }

        public async virtual Task<TModel> Add(TModel model)
        {
            return (await db.Set<TModel>().AddAsync(model)).Entity;
        }

        public async virtual Task AddRange(IEnumerable<TModel> models)
        {
            await db.Set<TModel>().AddRangeAsync(models);
        }

        public virtual TModel Update(TModel model)
        {
            return db.Set<TModel>().Update(model).Entity;
        }

        public async virtual Task<TModel> UpdateById(long id)
        {
            TModel model = await this.GetById(id);
            return this.Update(model);
        }

        /// <summary>
        /// Delete item in database
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Deleted model</returns>
        public virtual TModel Delete(TModel model)
        {
            return db.Set<TModel>().Remove(model).Entity;
        }

        public async virtual Task<TModel> DeleteById(long id)
        {
            TModel model = await this.GetById(id);
            return this.Delete(model);
        }

        /// <summary>
        /// Save data to database asynchronously
        /// </summary>
        /// <returns>Number of saved items</returns>
        public async Task<int> Save()
        {
            try
            {
                return await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return 0;
            }
        }

        public async Task<bool> IsExists(long id)
        {
            if (await this.GetAll().AnyAsync(x => x.Id == id))
            {
                return true;
            }
            return false;
        }
    }
}
