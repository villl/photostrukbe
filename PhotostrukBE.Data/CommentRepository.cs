﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data.Models;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class CommentRepository : CUDRepository<Comment>
    {
        public CommentRepository(DataContext db) : base(db)
        {
        }

        public IQueryable<CommentOutput> GetAll(long imageId)
        {
            return db.Set<Comment>()
                .Include(x => x.User)
                .Include(x => x.Like)
                .Where(x => x.ImageId == imageId)
                .Select(x => new CommentOutput
                {
                    Id = x.Id,
                    Text = x.Text,
                    CreatedAt = x.CreatedAt,
                    User = x.User,
                    Likes = x.Like.Where(e => e.IsLiked == true).Count(),
                    Dislikes = x.Like.Where(z => z.IsLiked == false).Count()
                });
        }

        public async override Task<Comment> GetById(long id)
        {
            Comment model = (await db.Comment
                .Include(x => x.User)
                .Include(x => x.Like)
                .ToListAsync().ConfigureAwait(false)).Find(x => x.Id == id);

            if (model == null)
            {
                throw new HttpStatusCodeException(string.Format("Entity {0} with given id doesn't exist.", typeof(Comment).Name), StatusCodes.Status404NotFound);
            }

            return model;
        }
    }
}
