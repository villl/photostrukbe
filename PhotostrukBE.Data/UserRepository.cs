﻿using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(DataContext db) : base(db)
        {
        }
    }
}
