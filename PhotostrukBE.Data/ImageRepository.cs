﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class ImageRepository : BasePhotostrukRepository<Image>
    {
        public ImageRepository(PhotostrukContext db) : base(db)
        {

        }

        public override List<Image> GetAll(string cultureInfo)
        {
            return db.Image.ToList();    
        }

        public async virtual Task<Image> GetById(long id)
        {
            Image model = await db.Image.FindAsync(id);

            if (model == null)
            {
                throw new HttpStatusCodeException(string.Format("Entity {0} with given id doesn't exist.", typeof(Image).Name), StatusCodes.Status404NotFound);
            }

            return model;
        }
    }
}
