﻿using Microsoft.AspNetCore.Http;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class PhotoRepository : CUDRepository<Photo>
    {
        public PhotoRepository(DataContext db) : base(db)
        {
        }

        public async Task<User> GetUserById(long userId)
        {
            return await db.User.FindAsync(userId);
        }

        public IQueryable<Photo> GetAll(long objectId)
        {
            return db.Set<Photo>()
                .Where(x => x.ObjectId == objectId);
        }
    }
}
