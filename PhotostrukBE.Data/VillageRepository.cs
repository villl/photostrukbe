﻿using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using PhotostrukBE.Common;
using PhotostrukBE.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace PhotostrukBE.Data
{
    public class VillageRepository : BasePhotostrukRepository<Village>
    {
        public VillageRepository(PhotostrukContext db) : base(db)
        {

        }

        public override List<Village> GetAll(string cultureInfo)
        {
            var villages = new List<Village>();
            string attributes = string.Empty;

            if (cultureInfo.Trim().ToUpper() == Const.GermanCultureInfo)
            {
                attributes = @" watercourse_german, altitude, landscape_charakter_german,
points_of_iterrest_german, story_german,
village_history.id, village_history.villade_id, middle_ages_german, early_modern_ages_german, 
nineteenth_century_german, state_before_1945_german, state_after_1945_german, state_1989_german, present_german,";
            }
            else
            {
                attributes = @" wattercourse, altitude, landscape_charakter, points_of_iterrest, story,
village_history.id, village_history.villade_id, middle_ages, early_modern_ages, nineteenth_century,
state_before_1945, state_after_1945, state_1989, present,";
            }

            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = string.Format(@"SELECT village.id, name_czech, name_german, district, administrative_municipality, cadastral_area,
 {0}
 period_start, period_end, x, y FROM village
INNER JOIN village_asoc_gps_time_period ON village.id = village_asoc_gps_time_period.village_id
INNER JOIN gps_time_period ON village_asoc_gps_time_period.gps_time_period_id = gps_time_period.id
INNER JOIN gps_time_period_asoc_gps_coordinate ON gps_time_period.id = gps_time_period_asoc_gps_coordinate.gps_time_period_id
INNER JOIN gps_coordinate ON gps_time_period_asoc_gps_coordinate.gps_coordinate_id = gps_coordinate.id
LEFT JOIN village_history ON village.id = village_history.villade_id", attributes) ;
                db.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var village = new Village()
                        {
                            Id = reader.GetInt32(0),
                            NameCzech = reader.GetString(1),
                            NameGerman = reader.GetString(2),
                            District = reader.GetString(3),
                            AdministrativeMunicipality = reader.GetString(4),
                            CadastralArea = reader.GetString(5),
                            Wattercourse = reader.GetString(6),
                            Altitude = reader.GetInt32(7),
                            LandscapeCharakter = reader.GetString(8),
                            PointsOfIterrest = reader.GetString(9),
                            Story = reader.GetString(10),
                            PeriodStart = ParseDateTime(reader[20]),
                            PeriodEnd = ParseDateTime(reader[21]),
                            Latitude = ParseDouble(reader[22]),
                            Longitude = ParseDouble(reader[23])
                        };

                        if (reader[11] != null && int.TryParse(reader[11].ToString(), out int result))
                        {
                            var vh = new VillageHistory(){};
                            vh.Id = reader.GetInt32(11);
                            vh.VilladeId = reader.GetInt32(12);
                            vh.MiddleAges = reader.GetString(13);
                            vh.EarlyModernAges = reader.GetString(14);
                            vh.NineteenthCentury = reader.GetString(15);
                            vh.StateBefore1945 = reader.GetString(16);
                            vh.StateAfter1945 = reader.GetString(17);
                            vh.State1989 = reader.GetString(18);
                            vh.Present = reader.GetString(19);

                            village.History = vh;
                        }
                        

                        //village.Images = new List<Image>();
                        //village.Areals = new List<Areal>();
                        villages.Add(village);
                    }
                }
            }
            //foreach (var item in villages)
            //{
            //    item.Images.AddRange(this.GetAsocImages(item.Id));
            //    item.Areals.AddRange(this.GetAsocAreals(item.Id));
            //}
            
            return villages;
        }

        public List<VillageImage> GetAsocImages()
        {
            var images = new List<VillageImage>();
                using (var command = db.Database.GetDbConnection().CreateCommand())
                {
                    //command.Parameters.Add(new MySqlParameter
                    //    {
                    //        ParameterName = "@ID",
                    //        MySqlDbType = MySqlDbType.Int32,
                    //        Value=villageId
                    //    });
                    command.CommandText = @"SELECT image.id, village.id, period_start, period_end, name, path FROM village
INNER JOIN village_asoc_image_in_time ON village.id = village_asoc_image_in_time.village_id
INNER JOIN image_in_time ON village_asoc_image_in_time.image_in_time_id = image_in_time.id
INNER JOIN image ON image_in_time.image_id = image.id";
                    db.Database.OpenConnection();
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var image = new VillageImage()
                            {
                                Id = reader.GetInt32(0),
                                VillageId = reader.GetInt32(1),
                                PeriodStart = ParseDateTime(reader[2]),
                                PeriodEnd = ParseDateTime(reader[3]),
                                Name = reader.GetString(4),
                                Path = reader.GetString(5)
                            };
                            images.Add(image);
                        }
                    }
            }

            return images;
        }

//        public List<Areal> GetAsocAreals()
//        {
//            var areals = new List<Areal>();
//                using (var command = db.Database.GetDbConnection().CreateCommand())
//                {
//                    //command.Parameters.Add(new MySqlParameter
//                    //    {
//                    //        ParameterName = "@ID",
//                    //        MySqlDbType = MySqlDbType.Int32,
//                    //        Value=villageId
//                    //    });
//                    command.CommandText = @"SELECT areal.id, description, description_german FROM village
//INNER JOIN village_asoc_areal ON village.id = village_asoc_areal.village_id
//INNER JOIN areal ON village_asoc_areal.areal_id = areal.id";
//                    db.Database.OpenConnection();
//                    using (var reader = command.ExecuteReader())
//                    {
//                        while (reader.Read())
//                        {
//                            var areal = new Areal()
//                            {
//                                Id = reader.GetInt32(0),
//                                Description = reader.GetString(1),
//                                DescriptionGerman = reader.GetString(2)
//                            };
//                            areals.Add(areal);
//                        }
//                    }
//            }
//            return areals;
//        }

        public IQueryable<VillageAsocGpsTimePeriod> GetAsocGpsTimePeriod()
        {
            

            return db.VillageAsocGpsTimePeriod;
        }

        public IQueryable<GpsTimePeriod> GetGpsTimePeriods()
        {
            return db.GpsTimePeriod;
        }
    }
}
