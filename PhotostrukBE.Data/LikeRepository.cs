﻿using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class LikeRepository : CUDRepository<Like>
    {
        public LikeRepository(DataContext db) : base(db)
        {
        }

        public IQueryable<Like> GetAll(long commentId)
        {
            return db.Set<Like>().Where(x => x.CommentId == commentId);
        }

        /// <summary>
        /// Check if current comment is liked by user
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Like GetLiked(long commentId, long userId)
        {
            return db.Like.FirstOrDefault(x => x.CommentId == commentId && x.UserId == userId);
        }

        public async Task<CommentOutput> GetCommentById(long commentId)
        {
            return (await db.Set<Comment>()
                .Include(x => x.User)
                .Include(x => x.Like)
                .Select(x => new CommentOutput
                {
                    Id = x.Id,
                    Text = x.Text,
                    CreatedAt = x.CreatedAt,
                    User = x.User,
                    Likes = x.Like.Where(e => e.IsLiked == true).Count(),
                    Dislikes = x.Like.Where(z => z.IsLiked == false).Count()
                })
                .ToListAsync())
                .FirstOrDefault(x => x.Id == commentId);
        }

        public bool IsCommentExist(long commentId)
        {
            return db.Comment.Any(x => x.Id == commentId);
        }
    }
}
