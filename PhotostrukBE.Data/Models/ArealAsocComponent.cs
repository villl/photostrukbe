﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ArealAsocComponent
    {
        public int ArealId { get; set; }
        public int ComponentId { get; set; }
    }
}
