﻿namespace PhotostrukBE.Data.Models
{
    public partial class GpsTimePeriodAsocGpsCoordinate
    {
        public int GpsTimePeriodId { get; set; }
        public int GpsCoordinateId { get; set; }
    }
}
