﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class Component : BaseModel
    {
        public int? ArealId { get; set; }
        public string Name { get; set; }
        public string TypeOfBuilding { get; set; }
        public string Subtype { get; set; }
        public float Length { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }
        public byte IsImportant { get; set; }
        public string Owners { get; set; }
        public string Personalities { get; set; }
        public string Exterior { get; set; }
        public string Interior { get; set; }
        public string Inventory { get; set; }
        public string StateAfter1945 { get; set; }
        public string StateAfter1989 { get; set; }
        public string Present { get; set; }
        public string Notice { get; set; }
        public string History { get; set; }
        public string NameGerman { get; set; }
        public string Degree { get; set; }
        public string Type { get; set; }
        public string MaterialRecycling { get; set; }
        public string DisposalOfResidues { get; set; }
        public string SurfaceAdjusment { get; set; }
        public string TerrainRelicts { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
