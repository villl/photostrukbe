﻿using PhotostrukBE.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data.Models
{
    public class VillageImage : BaseModel
    {
        public int VillageId { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
