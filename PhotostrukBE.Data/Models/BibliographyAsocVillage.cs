﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class BibliographyAsocVillage
    {
        public int BibliographyId { get; set; }
        public int VillageId { get; set; }
    }
}
