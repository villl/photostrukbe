﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageStatEvaluation
    {
        public int HistoryId { get; set; }
        public int Year { get; set; }
        public int CountOfHouses { get; set; }
        public int CountOfInhabitants { get; set; }
        public int CountOfCzechs { get; set; }
        public int CountOfGermans { get; set; }
    }
}
