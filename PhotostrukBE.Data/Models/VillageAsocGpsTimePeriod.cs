﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageAsocGpsTimePeriod
    {
        public int VillageId { get; set; }
        public int GpsTimePeriodId { get; set; }
    }
}
