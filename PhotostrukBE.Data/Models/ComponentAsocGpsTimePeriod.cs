﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ComponentAsocGpsTimePeriod
    {
        public int ComponentId { get; set; }
        public int GpsTimePeriodId { get; set; }
    }
}
