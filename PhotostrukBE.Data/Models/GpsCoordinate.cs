﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class GpsCoordinate : BaseModel
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
