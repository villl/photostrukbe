﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PhotostrukBE.Data.Models
{
    public partial class PhotostrukContext : DbContext
    {
        public PhotostrukContext()
        {
        }

        public PhotostrukContext(DbContextOptions<PhotostrukContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Areal> Areal { get; set; }
        public virtual DbSet<ArealAsocComponent> ArealAsocComponent { get; set; }
        public virtual DbSet<ArealAsocGpsTimePeriod> ArealAsocGpsTimePeriod { get; set; }
        public virtual DbSet<ArealAsocImageInTime> ArealAsocImageInTime { get; set; }
        public virtual DbSet<Bibliography> Bibliography { get; set; }
        public virtual DbSet<BibliographyAsocComponent> BibliographyAsocComponent { get; set; }
        public virtual DbSet<BibliographyAsocVillage> BibliographyAsocVillage { get; set; }
        public virtual DbSet<Component> Component { get; set; }
        public virtual DbSet<ComponentAsocGpsTimePeriod> ComponentAsocGpsTimePeriod { get; set; }
        public virtual DbSet<ComponentAsocImageInTime> ComponentAsocImageInTime { get; set; }
        public virtual DbSet<ComponentUsing> ComponentUsing { get; set; }
        public virtual DbSet<DestructionTransformation> DestructionTransformation { get; set; }
        public virtual DbSet<GpsCoordinate> GpsCoordinate { get; set; }
        public virtual DbSet<GpsTimePeriod> GpsTimePeriod { get; set; }
        public virtual DbSet<GpsTimePeriodAsocGpsCoordinate> GpsTimePeriodAsocGpsCoordinate { get; set; }
        public virtual DbSet<Image> Image { get; set; }
        public virtual DbSet<ImageInTime> ImageInTime { get; set; }
        public virtual DbSet<ImageTags> ImageTags { get; set; }
        public virtual DbSet<PostDepositionTransformation> PostDepositionTransformation { get; set; }
        public virtual DbSet<Village> Village { get; set; }
        public virtual DbSet<VillageAsocAreal> VillageAsocAreal { get; set; }
        public virtual DbSet<VillageAsocGpsTimePeriod> VillageAsocGpsTimePeriod { get; set; }
        public virtual DbSet<VillageAsocImageInTime> VillageAsocImageInTime { get; set; }
        public virtual DbSet<VillageExp> VillageExp { get; set; }
        public virtual DbSet<VillageHistory> VillageHistory { get; set; }
        public virtual DbSet<VillageNextNames> VillageNextNames { get; set; }
        public virtual DbSet<VillageStatEvaluation> VillageStatEvaluation { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Areal>(entity =>
            {
                entity.ToTable("areal");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(255)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<ArealAsocComponent>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("areal_asoc_component");

                entity.HasIndex(e => e.ArealId)
                    .HasName("areal_id");

                entity.HasIndex(e => e.ComponentId)
                    .HasName("component_id_2");

                entity.Property(e => e.ArealId)
                    .HasColumnName("areal_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ComponentId)
                    .HasColumnName("component_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ArealAsocGpsTimePeriod>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("areal_asoc_gps_time_period");

                entity.HasIndex(e => e.ArealId)
                    .HasName("areal_id");

                entity.HasIndex(e => e.GpsTimePeriodId)
                    .HasName("gps_time_period_id");

                entity.Property(e => e.ArealId)
                    .HasColumnName("areal_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GpsTimePeriodId)
                    .HasColumnName("gps_time_period_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ArealAsocImageInTime>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("areal_asoc_image_in_time");

                entity.HasIndex(e => e.ArealId)
                    .HasName("areal_id");

                entity.HasIndex(e => e.ImageInTimeId)
                    .HasName("image_in_time_id");

                entity.Property(e => e.ArealId)
                    .HasColumnName("areal_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ImageInTimeId)
                    .HasColumnName("image_in_time_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Bibliography>(entity =>
            {
                entity.ToTable("bibliography");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Author)
                    .IsRequired()
                    .HasColumnName("author")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Detail1)
                    .IsRequired()
                    .HasColumnName("detail_1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Detail2)
                    .IsRequired()
                    .HasColumnName("detail_2")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Detail3)
                    .IsRequired()
                    .HasColumnName("detail_3")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Detail4)
                    .IsRequired()
                    .HasColumnName("detail_4")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .HasColumnName("year")
                    .HasColumnType("int(255)");
            });

            modelBuilder.Entity<BibliographyAsocComponent>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("bibliography_asoc_component");

                entity.HasIndex(e => e.BibliographyId)
                    .HasName("bibliography_id");

                entity.HasIndex(e => e.ComponentId)
                    .HasName("component_id");

                entity.Property(e => e.BibliographyId)
                    .HasColumnName("bibliography_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ComponentId)
                    .HasColumnName("component_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<BibliographyAsocVillage>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("bibliography_asoc_village");

                entity.HasIndex(e => e.BibliographyId)
                    .HasName("bibliography_id");

                entity.HasIndex(e => e.VillageId)
                    .HasName("areal_id");

                entity.Property(e => e.BibliographyId)
                    .HasColumnName("bibliography_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VillageId)
                    .HasColumnName("village_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Component>(entity =>
            {
                entity.ToTable("component");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");


                entity.Property(e => e.Exterior).HasColumnName("exterior");


                entity.Property(e => e.Height)
                    .HasColumnName("height")
                    .HasComment("vyska");

                entity.Property(e => e.History)
                    .IsRequired()
                    .HasColumnName("history");

                entity.Property(e => e.Interior).HasColumnName("interior");


                entity.Property(e => e.Inventory).HasColumnName("inventory");

                entity.Property(e => e.IsImportant)
                    .HasColumnName("is_important")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasComment("delka");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NameGerman)
                    .IsRequired()
                    .HasColumnName("name_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Notice).HasColumnName("notice");


                entity.Property(e => e.Owners).HasColumnName("owners");


                entity.Property(e => e.Personalities).HasColumnName("personalities");


                entity.Property(e => e.Present).HasColumnName("present");


                entity.Property(e => e.StateAfter1945).HasColumnName("state_after_1945");


                entity.Property(e => e.StateAfter1989).HasColumnName("state_after_1989");


                entity.Property(e => e.Subtype)
                    .IsRequired()
                    .HasColumnName("subtype")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("podtyp ");


                entity.Property(e => e.TypeOfBuilding)
                    .IsRequired()
                    .HasColumnName("type_of_building")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("typ budovy");


                entity.Property(e => e.Width)
                    .HasColumnName("width")
                    .HasComment("sirka");
            });

            modelBuilder.Entity<ComponentAsocGpsTimePeriod>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("component_asoc_gps_time_period");

                entity.HasIndex(e => e.ComponentId)
                    .HasName("component_id");

                entity.HasIndex(e => e.GpsTimePeriodId)
                    .HasName("gps_time_period_id");

                entity.Property(e => e.ComponentId)
                    .HasColumnName("component_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GpsTimePeriodId)
                    .HasColumnName("gps_time_period_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ComponentAsocImageInTime>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("component_asoc_image_in_time");

                entity.HasIndex(e => e.ComponentId)
                    .HasName("component_id");

                entity.HasIndex(e => e.ImageInTimeId)
                    .HasName("image_in_time_id");

                entity.Property(e => e.ComponentId)
                    .HasColumnName("component_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ImageInTimeId)
                    .HasColumnName("image_in_time_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<ComponentUsing>(entity =>
            {
                entity.ToTable("component_using");

                entity.HasIndex(e => e.ComponentId)
                    .HasName("component_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ComponentId)
                    .HasColumnName("component_id")
                    .HasColumnType("int(11)")
                    .HasComment("id");

                entity.Property(e => e.WayOfUsing)
                    .IsRequired()
                    .HasColumnName("way_of_using")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("zpusob vyuziti");

                entity.Property(e => e.WayOfUsingGerman)
                    .IsRequired()
                    .HasColumnName("way_of_using_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .HasColumnName("year")
                    .HasColumnType("int(11)")
                    .HasComment("rok");
            });

            modelBuilder.Entity<DestructionTransformation>(entity =>
            {
                entity.ToTable("destruction_transformation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Degree)
                    .IsRequired()
                    .HasColumnName("degree")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("stupen zanikove transformace");

                entity.Property(e => e.DegreeGerman)
                    .IsRequired()
                    .HasColumnName("degree_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Demolition)
                    .IsRequired()
                    .HasColumnName("demolition")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("demolice");

                entity.Property(e => e.DemolitionGerman)
                    .IsRequired()
                    .HasColumnName("demolition_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DisposalOfResidues)
                    .IsRequired()
                    .HasColumnName("disposal_of_residues")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("nakladani se zbytky");

                entity.Property(e => e.DisposalOfResiduesGerman)
                    .IsRequired()
                    .HasColumnName("disposal_of_residues_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MaterialRecycling)
                    .IsRequired()
                    .HasColumnName("material_recycling")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("recyklace materialu");

                entity.Property(e => e.MaterialRecyclingGerman)
                    .IsRequired()
                    .HasColumnName("material_recycling_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SurfaceAdjustment)
                    .IsRequired()
                    .HasColumnName("surface_adjustment")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("uprava povrchu");

                entity.Property(e => e.SurfaceAdjustmentGerman)
                    .IsRequired()
                    .HasColumnName("surface_adjustment_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TerrainRelicts)
                    .IsRequired()
                    .HasColumnName("terrain_relicts")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("terenni relikty");

                entity.Property(e => e.TerrainRelictsGerman)
                    .IsRequired()
                    .HasColumnName("terrain_relicts_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("typ");

                entity.Property(e => e.TypeGerman)
                    .IsRequired()
                    .HasColumnName("type_german")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GpsCoordinate>(entity =>
            {
                entity.ToTable("gps_coordinate");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.X).HasColumnName("x");

                entity.Property(e => e.Y).HasColumnName("y");
            });

            modelBuilder.Entity<GpsTimePeriod>(entity =>
            {
                entity.ToTable("gps_time_period");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)")
                    .HasComment("id");

                entity.Property(e => e.PeriodEnd)
                    .HasColumnName("period_end")
                    .HasColumnType("date")
                    .HasComment("konec obdobi");

                entity.Property(e => e.PeriodStart)
                    .HasColumnName("period_start")
                    .HasColumnType("date")
                    .HasComment("zacatek obdobi");
            });

            modelBuilder.Entity<GpsTimePeriodAsocGpsCoordinate>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("gps_time_period_asoc_gps_coordinate");

                entity.HasIndex(e => e.GpsCoordinateId)
                    .HasName("gps_coordinate_id");

                entity.HasIndex(e => e.GpsTimePeriodId)
                    .HasName("gps_time_period_id");

                entity.Property(e => e.GpsCoordinateId)
                    .HasColumnName("gps_coordinate_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.GpsTimePeriodId)
                    .HasColumnName("gps_time_period_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.ToTable("image");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnName("path")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ImageInTime>(entity =>
            {
                entity.ToTable("image_in_time");

                entity.HasIndex(e => e.ImageId)
                    .HasName("image_id_2");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ImageId)
                    .HasColumnName("image_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PeriodEnd)
                    .HasColumnName("period_end")
                    .HasColumnType("date");

                entity.Property(e => e.PeriodStart)
                    .HasColumnName("period_start")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<ImageTags>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("image_tags");

                entity.HasIndex(e => e.ImageId)
                    .HasName("image_id");

                entity.Property(e => e.ImageId)
                    .HasColumnName("image_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Tag)
                    .IsRequired()
                    .HasColumnName("tag")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PostDepositionTransformation>(entity =>
            {
                entity.ToTable("post_deposition_transformation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cultural)
                    .IsRequired()
                    .HasColumnName("cultural");

                entity.Property(e => e.CulturalGerman)
                    .IsRequired()
                    .HasColumnName("cultural_german");

                entity.Property(e => e.Natural)
                    .IsRequired()
                    .HasColumnName("natural");

                entity.Property(e => e.NaturalGerman)
                    .IsRequired()
                    .HasColumnName("natural_german");
            });

            modelBuilder.Entity<Village>(entity =>
            {
                entity.ToTable("village");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AdministrativeMunicipality)
                    .IsRequired()
                    .HasColumnName("administrative_municipality")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("spravni obec");

                entity.Property(e => e.Altitude).HasColumnName("altitude");

                entity.Property(e => e.CadastralArea)
                    .IsRequired()
                    .HasColumnName("cadastral_area")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("katastrální obec");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("okres");

                entity.Property(e => e.LandscapeCharakter)
                    .IsRequired()
                    .HasColumnName("landscape_charakter")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''")
                    .HasComment("charakter krajiny");


                entity.Property(e => e.NameCzech)
                    .IsRequired()
                    .HasColumnName("name_czech")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("jmeno cesky");

                entity.Property(e => e.NameGerman)
                    .IsRequired()
                    .HasColumnName("name_german")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("jmeno nemecky");

                entity.Property(e => e.PointsOfIterrest)
                    .IsRequired()
                    .HasColumnName("points_of_iterrest")
                    .HasComment("zajímavosti");


                entity.Property(e => e.Story)
                    .IsRequired()
                    .HasColumnName("story");

                entity.Property(e => e.Wattercourse)
                    .IsRequired()
                    .HasColumnName("wattercourse")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("vodoteč");
            });

            modelBuilder.Entity<VillageAsocAreal>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("village_asoc_areal");

                entity.HasIndex(e => e.ArealId)
                    .HasName("areal_id");

                entity.HasIndex(e => e.VillageId)
                    .HasName("village_id");

                entity.Property(e => e.ArealId)
                    .HasColumnName("areal_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VillageId)
                    .HasColumnName("village_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<VillageAsocGpsTimePeriod>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("village_asoc_gps_time_period");

                entity.HasIndex(e => e.GpsTimePeriodId)
                    .HasName("gps_time_period_id");

                entity.HasIndex(e => e.VillageId)
                    .HasName("village_id");

                entity.Property(e => e.GpsTimePeriodId)
                    .HasColumnName("gps_time_period_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VillageId)
                    .HasColumnName("village_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<VillageAsocImageInTime>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("village_asoc_image_in_time");

                entity.HasIndex(e => e.ImageInTimeId)
                    .HasName("image_in_time_id");

                entity.HasIndex(e => e.VillageId)
                    .HasName("village_id");

                entity.Property(e => e.ImageInTimeId)
                    .HasColumnName("image_in_time_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.VillageId)
                    .HasColumnName("village_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<VillageExp>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("village_exp");

                entity.Property(e => e.District)
                    .IsRequired()
                    .HasColumnName("district")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("okres");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LandscapeCharakter)
                    .IsRequired()
                    .HasColumnName("landscape_charakter")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''")
                    .HasComment("charakter krajiny");

                entity.Property(e => e.NameCzech)
                    .IsRequired()
                    .HasColumnName("name_czech")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("jmeno cesky");

                entity.Property(e => e.NameGerman)
                    .IsRequired()
                    .HasColumnName("name_german")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasComment("jmeno nemecky");

                entity.Property(e => e.PointsOfIterrest)
                    .IsRequired()
                    .HasColumnName("points_of_iterrest")
                    .HasComment("zajímavosti");
            });

            modelBuilder.Entity<VillageHistory>(entity =>
            {
                entity.ToTable("village_history");

                entity.HasIndex(e => e.VilladeId)
                    .HasName("villade_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EarlyModernAges).HasColumnName("early_modern_ages");


                entity.Property(e => e.MiddleAges).HasColumnName("middle_ages");


                entity.Property(e => e.NineteenthCentury).HasColumnName("nineteenth_century");


                entity.Property(e => e.Present).HasColumnName("present");


                entity.Property(e => e.State1989).HasColumnName("state_1989");


                entity.Property(e => e.StateAfter1945).HasColumnName("state_after_1945");

                entity.Property(e => e.StateBefore1945).HasColumnName("state_before_1945");


                entity.Property(e => e.VilladeId)
                    .HasColumnName("villade_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<VillageNextNames>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("village_next_names");

                entity.HasIndex(e => e.VillageId)
                    .HasName("village_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VillageId)
                    .HasColumnName("village_id")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<VillageStatEvaluation>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("village_stat_evaluation");

                entity.HasIndex(e => e.HistoryId)
                    .HasName("history_id");

                entity.Property(e => e.CountOfCzechs)
                    .HasColumnName("count_of_czechs")
                    .HasColumnType("int(11)")
                    .HasComment("pocet cechu");

                entity.Property(e => e.CountOfGermans)
                    .HasColumnName("count_of_germans")
                    .HasColumnType("int(11)")
                    .HasComment("pocet nemcu");

                entity.Property(e => e.CountOfHouses)
                    .HasColumnName("count_of_houses")
                    .HasColumnType("int(11)")
                    .HasComment("pocet domu");

                entity.Property(e => e.CountOfInhabitants)
                    .HasColumnName("count_of_inhabitants")
                    .HasColumnType("int(11)")
                    .HasComment("pocet vsech lidi");

                entity.Property(e => e.HistoryId)
                    .HasColumnName("history_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Year)
                    .HasColumnName("year")
                    .HasColumnType("int(11)")
                    .HasComment("rok");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
