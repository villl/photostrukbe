﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ComponentAsocImageInTime
    {
        public int ComponentId { get; set; }
        public int ImageInTimeId { get; set; }
    }
}
