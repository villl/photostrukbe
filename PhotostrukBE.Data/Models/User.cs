﻿using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public class User : BaseModel
    {
        public User()
        {
            Photo = new HashSet<Photo>();
            Comment = new HashSet<Comment>();
            Like = new HashSet<Like>();
            Tag = new HashSet<Tag>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Provider { get; set; }
        public virtual ICollection<Photo> Photo { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<Like> Like { get; set; }
        public virtual ICollection<Tag> Tag { get; set; }
    }
}
