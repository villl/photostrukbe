﻿namespace PhotostrukBE.Data.Models
{
    public partial class ArealAsocGpsTimePeriod
    {
        public int ArealId { get; set; }
        public int GpsTimePeriodId { get; set; }
    }
}
