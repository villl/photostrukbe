﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class DestructionTransformation : BaseModel
    {
        public string Degree { get; set; }
        public string Type { get; set; }
        public string Demolition { get; set; }
        public string MaterialRecycling { get; set; }
        public string DisposalOfResidues { get; set; }
        public string SurfaceAdjustment { get; set; }
        public string TerrainRelicts { get; set; }
        public string DegreeGerman { get; set; }
        public string TypeGerman { get; set; }
        public string DemolitionGerman { get; set; }
        public string MaterialRecyclingGerman { get; set; }
        public string DisposalOfResiduesGerman { get; set; }
        public string SurfaceAdjustmentGerman { get; set; }
        public string TerrainRelictsGerman { get; set; }
    }
}
