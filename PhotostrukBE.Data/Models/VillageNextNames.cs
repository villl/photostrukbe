﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageNextNames
    {
        public int VillageId { get; set; }
        public string Name { get; set; }
    }
}
