﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ImageInTime : BaseModel
    {
        public DateTime? PeriodStart { get; set; }
        public DateTime? PeriodEnd { get; set; }
        public int ImageId { get; set; }
    }
}
