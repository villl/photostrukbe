﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageHistory : BaseModel
    {
        public int VilladeId { get; set; }
        public string MiddleAges { get; set; }
        public string EarlyModernAges { get; set; }
        public string NineteenthCentury { get; set; }
        public string StateBefore1945 { get; set; }
        public string StateAfter1945 { get; set; }
        public string State1989 { get; set; }
        public string Present { get; set; }
    }
}
