﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data.Models
{
    public class Photo : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Path { get; set; }
        public string PublicId { get; set; }
        public string Url { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long ObjectId { get; set; }
        public long UserId { get; set; }
        public virtual User User { get; set; }
    }
}
