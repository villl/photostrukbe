﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class PostDepositionTransformation : BaseModel
    {
        public string Cultural { get; set; }
        public string Natural { get; set; }
        public string CulturalGerman { get; set; }
        public string NaturalGerman { get; set; }
    }
}
