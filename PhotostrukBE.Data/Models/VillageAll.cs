﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data.Models
{
    public class VillageAll : BaseModel
    {
        public string NameCzech { get; set; }
        public string NameGerman { get; set; }
        public string District { get; set; }
        public string AdministrativeMunicipality { get; set; }
        public string CadastralArea { get; set; }
        public string Wattercourse { get; set; }
        public float Altitude { get; set; }
        public string LandscapeCharakter { get; set; }
        public string PointsOfIterrest { get; set; }
        public string WatercourseGerman { get; set; }
        public string LandscapeCharakterGerman { get; set; }
        public string PointsOfIterrestGerman { get; set; }
        public string Story { get; set; }
        public string StoryGerman { get; set; }
        public DateTimeOffset PeriodStart { get; set; }
        public DateTimeOffset PeriodEnd { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
