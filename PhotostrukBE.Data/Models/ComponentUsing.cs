﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ComponentUsing : BaseModel
    {
        public int ComponentId { get; set; }
        public int Year { get; set; }
        public string WayOfUsing { get; set; }
        public string WayOfUsingGerman { get; set; }
    }
}
