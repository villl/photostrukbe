﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageAsocAreal
    {
        public int VillageId { get; set; }
        public int ArealId { get; set; }
    }
}
