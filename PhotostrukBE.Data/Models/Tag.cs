﻿using System;

namespace PhotostrukBE.Data.Models
{
    public class Tag : BaseModel
    {
        public string Name { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long ImageId { get; set; }
        public long UserId { get; set; }
        public virtual User User { get; set; }
    }
}
