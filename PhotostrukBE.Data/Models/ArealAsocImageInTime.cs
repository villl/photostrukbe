﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ArealAsocImageInTime
    {
        public int ArealId { get; set; }
        public int ImageInTimeId { get; set; }
    }
}
