﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class ImageTags
    {
        public long ImageId { get; set; }
        public string Tag { get; set; }
    }
}
