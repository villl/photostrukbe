﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageAsocImageInTime
    {
        public int VillageId { get; set; }
        public int ImageInTimeId { get; set; }
    }
}
