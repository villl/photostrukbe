﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class Image : BaseModel
    {
        
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
