﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class BibliographyAsocComponent
    {
        public int BibliographyId { get; set; }
        public int ComponentId { get; set; }
    }
}
