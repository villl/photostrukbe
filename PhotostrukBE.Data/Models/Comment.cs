﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data.Models
{
    public class Comment : BaseModel
    {
        public Comment()
        {
            Like = new HashSet<Like>();
        }
        public string Text { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long ImageId { get; set; }
        public long? ReferenceTo { get; set; }
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Like> Like { get; set; }
    }
}
