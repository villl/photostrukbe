﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data.Models
{
    public class CommentOutput
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public User User { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }
}
