﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class VillageExp : BaseModel
    {
        public string NameCzech { get; set; }
        public string NameGerman { get; set; }
        public string District { get; set; }
        public string LandscapeCharakter { get; set; }
        public string PointsOfIterrest { get; set; }
    }
}
