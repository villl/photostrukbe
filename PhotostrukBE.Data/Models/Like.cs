﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhotostrukBE.Data.Models
{
    public class Like : BaseModel
    {
        public bool IsLiked { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public long CommentId { get; set; }
        public virtual Comment Comment { get; set; }
    }
}
