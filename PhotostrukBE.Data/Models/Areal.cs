﻿using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data.Models
{
    public partial class Areal : BaseModel
    {
        public int? VillageId { get; set; }
        public string Description { get; set; }
        public string Degree { get; set; }
        public string Type { get; set; }
        public string MaterialRecycling { get; set; }
        public string DisposalOfResidues { get; set; }
        public string SurfaceAdjusment { get; set; }
        public string TerrainRelicts { get; set; }
        public DateTimeOffset? PeriodStart { get; set; }
        public DateTimeOffset? PeriodEnd { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
