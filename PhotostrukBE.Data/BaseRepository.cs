﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Common.Exceptions;
using PhotostrukBE.Data.Interfaces;
using PhotostrukBE.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class BaseRepository<TModel> : BaseRepository where TModel : class, IBaseModel
    {
        public BaseRepository(DataContext db) : base(db)
        {

        }

        public virtual IQueryable<TModel> GetAll()
        {
            return db.Set<TModel>();
        }

        public async virtual Task<TModel> GetById(long id)
        {
            TModel model = (await GetAll().ToListAsync().ConfigureAwait(false)).Find(x => x.Id == id);

            if (model == null)
            {
                throw new HttpStatusCodeException(string.Format("Entity {0} with given id doesn't exist.", typeof(TModel).Name), StatusCodes.Status404NotFound);
            }

            return model;
        }

    }

    public abstract class BaseRepository
    {
        protected readonly DataContext db;
        public BaseRepository(DataContext db)
        {
            this.db = db;
        }
    }
}
