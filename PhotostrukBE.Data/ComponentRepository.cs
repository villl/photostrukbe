﻿using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Common;
using PhotostrukBE.Data.Models;
using System.Collections.Generic;

namespace PhotostrukBE.Data
{
    public class ComponentRepository : BasePhotostrukRepository<Component>
    {
        public ComponentRepository(PhotostrukContext db) : base(db)
        {
        }

        public override List<Component> GetAll(string cultureInfo)
        {
            var components = new List<Component>();
            string attributes = string.Empty;

            if (cultureInfo.Trim().ToUpper() == Const.GermanCultureInfo)
            {
                attributes = @" type_of_building_german, subtype_german,
owners_german, personalities_german, exterior_german, interior_german, inventory_german, state_after_1945_german,
state_after_1989_german, present_german, notice_german, history_german,
degree_german, type_german, material_recycling_german, disposal_of_residues_german, surface_adjustment_german, terrain_relicts_german,";
            }
            else
            {
                attributes = @" type_of_building, subtype,
owners, personalities, exterior, 
interior, inventory, state_after_1945, state_after_1989, present, notice, history,
degree, type, material_recycling, disposal_of_residues, surface_adjustment, terrain_relicts,";
            }

            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = string.Format(@"SELECT component.id, name, length, width, height, is_important,  name_german,
{0}
areal_id, period_start, period_end, x, y 
FROM component
INNER JOIN component_asoc_gps_time_period ON component.id = component_asoc_gps_time_period.component_id
INNER JOIN gps_time_period ON component_asoc_gps_time_period.gps_time_period_id = gps_time_period.id
INNER JOIN gps_time_period_asoc_gps_coordinate ON gps_time_period.id = gps_time_period_asoc_gps_coordinate.gps_time_period_id
INNER JOIN gps_coordinate ON gps_time_period_asoc_gps_coordinate.gps_coordinate_id = gps_coordinate.id
LEFT JOIN destruction_transformation ON component.destruction_transformation_id = destruction_transformation.id
LEFT JOIN areal_asoc_component ON component.id = areal_asoc_component.component_id
", attributes);
                db.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var component = new Component()
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1),
                            Length = reader.GetInt32(2),
                            Width = reader.GetInt32(3),
                            Height = reader.GetInt32(4),
                            IsImportant = reader.GetByte(5),
                            NameGerman = reader.GetString(6),
                            TypeOfBuilding = reader.GetString(7),
                            Subtype = reader.GetString(8),
                            Owners = reader.GetString(9),
                            Personalities = reader.GetString(10),
                            Exterior = reader.GetString(11),
                            Interior = reader.GetString(12),
                            Inventory = reader.GetString(13),
                            StateAfter1945 = reader.GetString(14),
                            StateAfter1989 = reader.GetString(15),
                            Present = reader.GetString(16),
                            Notice = reader.GetString(17),
                            History = reader.GetString(18),
                            Degree = reader.GetString(19),
                            Type = reader.GetString(20),
                            MaterialRecycling = reader.GetString(21),
                            DisposalOfResidues = reader.GetString(22),
                            SurfaceAdjusment = reader.GetString(23),
                            TerrainRelicts = reader.GetString(24),
                            ArealId = ParseInt(25),
                            PeriodStart = ParseDateTime(reader[26]),
                            PeriodEnd = ParseDateTime(reader[27]),
                            Latitude = ParseDouble(reader[28]),
                            Longitude = ParseDouble(reader[29])
                        };
                        components.Add(component);
                    }
                }
            }

            return components;
        }

        public List<ComponentImage> GetAsocImages()
        {
            var images = new List<ComponentImage>();
            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                //command.Parameters.Add(new MySqlParameter
                //    {
                //        ParameterName = "@ID",
                //        MySqlDbType = MySqlDbType.Int32,
                //        Value=villageId
                //    });
                command.CommandText = @"SELECT image.id, component.id, period_start, period_end, image.name, path FROM component
INNER JOIN component_asoc_image_in_time ON component.id = component_asoc_image_in_time.component_id
INNER JOIN image_in_time ON component_asoc_image_in_time.image_in_time_id = image_in_time.id
INNER JOIN image ON image_in_time.image_id = image.id";
                db.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var image = new ComponentImage()
                        {
                            Id = reader.GetInt32(0),
                            ComponentId = reader.GetInt32(1),
                            PeriodStart = ParseDateTime(reader[2]),
                            PeriodEnd = ParseDateTime(reader[3]),
                            Name = reader.GetString(4),
                            Path = reader.GetString(5)
                        };
                        images.Add(image);
                    }
                }
            }

            return images;
        }
    }
}
