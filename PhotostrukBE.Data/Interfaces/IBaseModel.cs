﻿using System.ComponentModel.DataAnnotations.Schema;

namespace PhotostrukBE.Data.Interfaces
{
    public interface IBaseModel
    {
       long Id { get; set; }
    }
}
