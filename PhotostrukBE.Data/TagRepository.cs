﻿using PhotostrukBE.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace PhotostrukBE.Data
{
    public class TagRepository : CUDRepository<Tag>
    {
        protected readonly PhotostrukContext dbPhotostruk;
        public TagRepository(DataContext db, PhotostrukContext dbPhotostruk) : base(db)
        {
            this.dbPhotostruk = dbPhotostruk;
        }

        public IQueryable<Tag> GetAll(long imageId)
        {
            return db.Set<Tag>()
                .Where(x => x.ImageId == imageId);
        }

        public List<Tag> GetPhotostrukAll(long imageId)
        {
            //var result = new List<Tag>();
            var imageTagsList = dbPhotostruk.Set<ImageTags>()
               .Where(x => x.ImageId == imageId)
               .Select(x => new Tag
               {
                   Id = x.ImageId,
                   Name = x.Tag
               })
               .ToList();

            //foreach (var item in imageTagsList)
            //{
            //    var tag = new Tag();
            //    tag.Id = item.ImageId;
            //    tag.Name = item.Tag;
            //    result.Add(tag);
            //}
            return imageTagsList;
        }
    }
}
