﻿using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Common;
using PhotostrukBE.Data.Models;
using System.Collections.Generic;

namespace PhotostrukBE.Data
{
    public class ArealRepository : BasePhotostrukRepository<Areal>
    {
        public ArealRepository(PhotostrukContext db) : base(db)
        {

        }

        public override List<Areal> GetAll(string cultureInfo)
        {
            var areals = new List<Areal>();
            string attributes = string.Empty;

            if (cultureInfo.Trim().ToUpper() == Const.GermanCultureInfo)
            {
                attributes = @" description_german, degree_german, type_german, material_recycling_german,
disposal_of_residues_german, surface_adjustment_german, terrain_relicts_german,";
            }
            else
            {
                attributes = @" description, degree, type, material_recycling, 
disposal_of_residues, surface_adjustment, terrain_relicts,";
            }

            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = string.Format(@"SELECT areal.id, village_id,  
{0}
 period_start, period_end, x, y FROM areal
LEFT JOIN areal_asoc_gps_time_period ON areal.id = areal_asoc_gps_time_period.areal_id
LEFT JOIN gps_time_period ON areal_asoc_gps_time_period.gps_time_period_id = gps_time_period.id
LEFT JOIN gps_time_period_asoc_gps_coordinate ON gps_time_period.id = gps_time_period_asoc_gps_coordinate.gps_time_period_id
LEFT JOIN gps_coordinate ON gps_time_period_asoc_gps_coordinate.gps_coordinate_id = gps_coordinate.id
LEFT JOIN destruction_transformation ON areal.destruction_transformation_id = destruction_transformation.id
LEFT JOIN village_asoc_areal ON areal.id = village_asoc_areal.areal_id", attributes) ;
                db.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var areal = new Areal();
                        areal.Id = reader.GetInt32(0);
                        areal.VillageId = ParseInt(reader[1]);
                        areal.Description = reader.GetString(2);
                        areal.Degree = reader.GetString(3);
                        areal.Type = reader.GetString(4);
                        areal.MaterialRecycling = reader.GetString(5);
                        areal.DisposalOfResidues = reader.GetString(6);
                        areal.SurfaceAdjusment = reader.GetString(7);
                        areal.TerrainRelicts = reader.GetString(8);
                        areal.PeriodStart = ParseDateTime(reader[9]);
                        areal.PeriodEnd = ParseDateTime(reader[10]);
                        areal.Latitude = ParseDouble(reader[11]);
                        areal.Longitude = ParseDouble(reader[12]);
                        areals.Add(areal);
                    }
                }
            }

            return areals;
        }

        public List<ArealImage> GetAsocImages()
        {
            var images = new List<ArealImage>();
            using (var command = db.Database.GetDbConnection().CreateCommand())
            {
                //command.Parameters.Add(new MySqlParameter
                //    {
                //        ParameterName = "@ID",
                //        MySqlDbType = MySqlDbType.Int32,
                //        Value=villageId
                //    });
                command.CommandText = @"SELECT image.id, areal.id, period_start, period_end, name, path FROM areal
INNER JOIN areal_asoc_image_in_time ON areal.id = areal_asoc_image_in_time.areal_id
INNER JOIN image_in_time ON areal_asoc_image_in_time.image_in_time_id = image_in_time.id
INNER JOIN image ON image_in_time.image_id = image.id";
                db.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var image = new ArealImage()
                        {
                            Id = reader.GetInt32(0),
                            ArealId = reader.GetInt32(1),
                            PeriodStart = ParseDateTime(reader[2]),
                            PeriodEnd = ParseDateTime(reader[3]),
                            Name = reader.GetString(4),
                            Path = reader.GetString(5)
                        };
                        images.Add(image);
                    }
                }
            }

            return images;
        }
    }
}
