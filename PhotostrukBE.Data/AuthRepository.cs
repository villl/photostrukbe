﻿using Microsoft.EntityFrameworkCore;
using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PhotostrukBE.Data
{
    public class AuthRepository : CUDRepository<User>
    {
        public AuthRepository(DataContext db) : base(db) { }

        public async Task<bool> IsUserExists(string email)
        {
            if (await this.GetAll().AnyAsync(u => u.Email == email.ToLower()))
            {
                return true;
            }

            return false;
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await this.GetAll().FirstOrDefaultAsync(u => u.Email == email.ToLower());
        }
    }
}
