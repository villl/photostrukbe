﻿using PhotostrukBE.Data.Interfaces;
using PhotostrukBE.Data.Models;
using System;
using System.Collections.Generic;

namespace PhotostrukBE.Data
{
    public abstract class BasePhotostrukRepository<TModel> : BasePhotostrukRepository 
        where TModel : class, IBaseModel
    {
        
        public BasePhotostrukRepository(PhotostrukContext db) : base(db)
        {
        }

        public abstract List<TModel> GetAll(string cultureInfo);

        public int? ParseInt(object input)
        {
            if (int.TryParse(input.ToString(), out int result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        public double? ParseDouble(object input)
        {
            if (double.TryParse(input.ToString(), out double result))
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        public DateTimeOffset? ParseDateTime(object input)
        {
            try
            {
                return DateTimeOffset.Parse(input.ToString());
            }
            catch (FormatException)
            {
                return null;
            }
        }
    }

    public abstract class BasePhotostrukRepository
    {
        protected readonly PhotostrukContext db;
        public BasePhotostrukRepository(PhotostrukContext db)
        {
            this.db = db;
        }
    }
}
